import sqlalchemy as db

engine = db.create_engine('sqlite:///tarea1.db')
conn = engine.connect()
metadata = db.MetaData()
persona = db.Table('asig1', metadata, autoload=True, autoload_with=engine)

class Palabra(ModeloBase):
    PALABRA = TextField()
    SIGNIFICADO1 = TextField()

def crear_tablas():
    conn.connect()
    conn.create_tables([Palabra])

class ModeloBase(Model):
    class Meta:
        database = conn

def principal():
    crear_tablas()
    MENU ==="""

a) Agregar Palabra.
b) Editar Palabr existente.
c) Eliminar  Palabra existente.
d) Ver  Palabras.
e) Buscar un significado  Palabra.
f) exit.

Elige: """

    elec = ""
    while elec != "f":
        elec = input(MENU)

        if elec == "a":
            palabra = input("Ingrese la Palabra porfavor: ")

            old_significado = buscar_significado_palabra(PALABRA)

            if old_significado:
                print(f"La Palabra '{PALABRA}' ya existe!")
            else:
                SIGNIFICADO1 = input("Ingrese el Significado porfavor: ")
                agregarp(PALABRA, SIGNIFICADO1)
                print("La Palabra ha sido agregada satisfactoriamente!!")

        if elec == "b":
            PALABRA = input("Ingrese la Palabra que desea editar: ")
            newsignificado = input("Ingrese el nuevo significado de la misma: ")
            editarp(PALABRA, newsignificado)
            print("La Palabra ha sido actualizada correctamente!!")

        if elec == "c":
            PALABRA = input("Ingrese la Palabra que desea eliminar por favor: ")
            eliminarp(PALABRA)

        if elec == "d":
            palabras = obtenerp()
            print("=== Lista de palabras ===")
            for PALABRA in palabras:
                print(PALABRA[0])

        if elec == "e":
            PALABRA = input(
                "Ingrese la Palabra de la cual desea saber el significado por favor: ")
            SIGNIFICADO1= buscar_significado_palabra(PALABRA)
            if SIGNIFICADO1:
                print(
                    f"El Significado de '{PALABRA}' es:\n{SIGNIFICADO1}")
            else:
                print(f"La Palabra '{PALABRA}' no fue encontrada")



def agregarp(PALABRA, SIGNIFICADO1):
    Palabra.create(palabra=PALABRA, significado=SIGNIFICADO1)


def editarp(PALABRA, newsignificado):
    Palabra.update({Palabra.significado: newsignificado}).where(
        Palabra.palabra == palabra).execute()

def obtenerp():
    return Palabra.select()

def eliminarp(PALABRA):
    Palabra.delete().where(Palabra.palabra == palabra).execute()

def buscar_significado_palabra(PALABRA):
    
    try:
        return Palabra.select().where(Palabra.palabra == palabra).get().significado
    except Exception:

if __name__ == '__main__':
    principal()


conn.commit()

conn.close()

